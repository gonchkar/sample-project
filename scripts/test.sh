#!/usr/bin/env bash

# Web Page of BASH best practices https://kvz.io/blog/2013/11/21/bash-best-practices/
#Exit when a command fails.
set -o errexit
#Exit when script tries to use undeclared variables.
set -o nounset
#The exit status of the last command that threw a non-zero exit code is returned.
set -o pipefail

#Trace what gets executed. Useful for debugging.
#set -o xtrace

# Set magic variables for current file & dir
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
__base="$(basename ${__file} .sh)"
__root="$(cd "$(dirname "${__dir}")" && pwd)"

source common.sh

#Go to main folder
cd ..

if [ ! -d ${BUILD_FOLDER} ]; then
    echo "ERROR: target folder does not exits".
    exit 1
fi

for i in `seq 1 ${TOTAL_FILE}`;
do
    if [ ! -f ${BUILD_FOLDER}/${FILE_NAME}${i}.txt ]; then
        echo "ERROR: File file{i}.txt does not exist."
        exit 1
    fi
done

echo "Test passed successfully."
exit 0